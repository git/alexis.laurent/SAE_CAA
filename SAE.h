#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


typedef struct maillon
{
    char departement[30];
    int nbPers;
    char responsable[30];
    struct maillon *suiv;
}MaillonDept;

typedef MaillonDept* ListeDept;

typedef struct
{
    char ville[30];
    MaillonDept *idDept;
}VilleIUT;

//#########################################
//#########################################
//#########################################

typedef struct
{
    char departement[30];
    int decisionDept;
    int decisionCandid;
}ChoixDept;

typedef struct maillonEtu
{
    char iutCandid[30];
    int nbChoix;
    ChoixDept **tabDept;
    struct maillonEtu *suiv;
}MaillonCandid, *ListeCandid;

typedef struct
{
    int id;
    char nom[30];
    char prenom[30];
    float note[4];
    MaillonCandid *idCandIUT;
}Candidat;

//#########################################
//#########################################
//#########################################

bool motdepasseVerif(void);

bool verifQuit(void);

int menuUtilisateurAffiche(void);

int menuAdminAffiche(void);

void gestionMenu(VilleIUT **tiut, int nbEle, Candidat **tcandid, int *nbCandid, int *taillePCandid);

int gestionMenuAdmin(VilleIUT **tiut, int nbEle);

void global(void);

void AffichageVille(VilleIUT **tiut, int nbEle);

void AffichageGlobalDepVille(VilleIUT **tiut, int nbEle);

void AffichageNbplaceDept(VilleIUT **tiut, int nbEle);

int rechVille(VilleIUT **tiut, int nbEle, char code[], int *trouve);

void AffichageNbplaceDeptVille(VilleIUT **tiut, int nbEle);

void AffichageDepVille(VilleIUT **tiut, int nbEle);

void AffichageDeptVille(VilleIUT **tiut, int nbEle);

void AffichageGlobalIUT(VilleIUT **tiut, int nbEle);

void MiseAJourNbPlace(VilleIUT **tiut, int nbEle);

void creationDept(VilleIUT **tiut, int nbEle);

void suppressionDept(VilleIUT **tiut, int nbEle);

void modifChefDept(VilleIUT **tiut, int nbEle);

void triEchange(VilleIUT **tiut, int nbEle);

int plusGrand(VilleIUT **tiut, int nbEle);

void echanger(VilleIUT **tiut, int i, int j);

void clearpage(void);

ListeDept creerListeDept(void);

VilleIUT **chargementVille(char *nomFich, int tphys, int *tailleL);

VilleIUT lireVille(FILE *flot);

ListeDept traiterDept(ListeDept l, FILE *flot);

ListeDept ajouterEnTeteDept(ListeDept l, char *dept, char *nom, int nbP);

void affichageListeDept(ListeDept l);

ListeDept insererDept(ListeDept l, char *dept, char *nom, int nbP);

ListeDept supprimerEnTêteDept(ListeDept l);

ListeDept supprimerDept(ListeDept l, char *code);

bool vide(ListeDept l);

ListeDept rechercheDept(ListeDept l, char code[]);

int longueurListe(ListeDept l);

void SauvegarderIUT(VilleIUT** tabV, int tailleL);

void SauvegarderListe(ListeDept l, FILE* flot);

//#######################################################
//#######################################################
//#######################################################

ListeCandid ListeCandidvide(void);

ListeCandid insererEnTeteCandid(ListeCandid l, char *ville, ChoixDept **tDept, int nbEle);

ListeCandid insererCandid(ListeCandid l, char *ville, ChoixDept **tDept, int nbEle);

int rechCandiDept(ChoixDept **tDept, int tailleL, char dept[], int *trouve);

int plusGrandCandidDept(ChoixDept **tDept, int tailleL);

void echangerCandidDept(ChoixDept **tDept, int i, int j);

void triEchangeCandidDept(ChoixDept **tDept, int tailleL);

void videTabDept(ChoixDept **tDept, int tailleP);

ListeCandid suppressionEnTeteCandid(ListeCandid l , ChoixDept **tDept, int tailleP);

ListeCandid suppressionCandid(ListeCandid l, char *ville, ChoixDept **tDept, int tailleP);

void afficherCandid(Candidat **tab, int nbEle);

void afficherCandidIUT(ListeCandid l);

void afficherCandidDept(ChoixDept **tab, int nbChoix);

int longueur(ListeCandid l);

Candidat **chargmentCandid(char *nomFich, int *tphys, int *tailleL);

Candidat lireCandidat(FILE *flot);

ListeCandid traiterCandidIUT(ListeCandid l, FILE *flot);

ChoixDept *traiterCandidDept(int tailleL, int tailleP, FILE *flot);

int rechCandid(Candidat **tab, int nbEle, int id, int *trouve);

void triEchangecandid(Candidat **tab, int nbEle);

int plusGrandCandid(Candidat **tab, int nbEle);

void echangerCandid(Candidat **tab, int i, int j);

ListeCandid rechCandidIUT(ListeCandid l, char *code);

void supprimerCandid(Candidat **tab, int *nbEle);

Candidat **reallocTCandid(Candidat **tab, int *tailleP);

void creationCandid(VilleIUT **tiut, int nbIUT, Candidat **tcandid, int *nbCandid, int *taillePCandid);

int menuCandid(void);

void afficher1Candid(Candidat **tab, int nbCandid);

void gestionCandid(VilleIUT **tiut, int nbIUT, Candidat **tcandid, int *nbCandid, int *taillePCandid);

int menuGestionCandid(void);

void decalagenom(char* tab, int pos, int nbEle);

ListeCandid recherchecandidIUT(ListeCandid l, char code[]);

int recherchecandidDept(ChoixDept **tab,int nbEle, char code[],int *trouve);

ChoixDept RempTabCandid(char dept[],int deciCandid ,int deciDept);

void ajouterDept(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid);

void supprimerCandidDept(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid);

void modifCandid(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid);

ListeDept creerListeDept(void);

ListeDept ajouterEnTeteDept(ListeDept l, char* dept, char* nom, int nbP);

ListeDept insererDept(ListeDept l, char *dept, char *nom, int nbP);

void affichageListeDept(ListeDept l);

VilleIUT lireVille(FILE *flot);

ListeDept traiterDept(ListeDept l, FILE *flot);

VilleIUT** chargementVille(char* nomFich, int tphys, int* tailleL);

ListeDept supprimerDept(ListeDept l, char *code);

int longueurListe(ListeDept l);

void SauvegarderIUT(VilleIUT** tabV, int tailleL);

void SauvegarderListe(ListeDept l, FILE* flot);

void SauvegardeCandid(Candidat **tabC, int nbCandid);

void SauvegarderCandidIUT(ListeCandid l, FILE *flot);

void SauvegardeCandidDept(ChoixDept **tDept, int nbDept, FILE *flot);

void decalageGauche(Candidat **tab,int pos, int nbEle);

void decalageGaucheDept(ChoixDept **tab, int pos, int nbEle);

int verifChefDepart(VilleIUT **tiut, int nbEle, char *dept, char *ville, char *nom);

int menuResponsableAffiche(void);

int gestionResponsable(VilleIUT **tiut, int nbEle, Candidat **tcandid, int tailleL);

int traiterCandidIUTDept(Candidat *candid, char *dept, char *ville);

Candidat** candidDept(Candidat** tabCandidat, char* dept, char* ville, int tailleL, int* nb);

void SauvegardeCandidAdmis(Candidat **tab, int nb, int admis);

void examinerCandid(Candidat **tabCandid, int nb, char* dept, char* ville);

//#######################################################
//#######################################################
//#######################################################

void afficherAdmiCandid(Candidat **tab,int nbCandid);

void afficher1AdmiCandid(Candidat **tab,int nbCandid, int id, int pos);

void rejeterVoeux(ListeCandid l, char ville[], int pos);

int decision(void);

void gererAdmiCandid(Candidat **tab, int nbCandid);