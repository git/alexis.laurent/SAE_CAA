#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "SAE.h"




// PARTIE 3

int traiterCandidIUTDept(Candidat* candid,char* dept, char* ville);

Candidat** candidDept(Candidat** tabCandidat, char* dept, char* ville, int tailleL, int* nb);

void SauvegardeCandidAdmis(Candidat** tab, int nb, int admis);

void examinerCandid(Candidat **tabCandid, int nb, char* dept, char* ville);

int gestionResponsable(VilleIUT **tiut, int nbEle, Candidat** tcandid, int tailleL);

int verifChefDepart(VilleIUT** tiut, int nbEle, char* dept, char* ville, char* nom);

int menuResponsableAffiche(void);

int gestionResponsable(VilleIUT **tiut, int nbEle, Candidat** tcandid, int tailleL);

