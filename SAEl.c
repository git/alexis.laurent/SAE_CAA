#include "SAEl.h"

int verifChefDepart(VilleIUT **tiut, int nbEle, char *dept, char *ville, char *nom)
{
    int i, verif = 0;
    ListeDept l;
    for (i = 0; i < nbEle; i++)
    {
        if (strcmp(tiut[i]->ville, ville) == 0)
        {
            l = tiut[i]->idDept;
            while (l != NULL)
            {
                if (strcmp(l->departement, dept) == 0)
                {
                    if (strcmp(l->responsable, nom) == 0)
                    {
                        verif = 1;
                    }
                }
                l = l->suiv;
            }
        }
    }
    return verif;
}

int menuResponsableAffiche(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Menu Responsable|\n\n");
    printf("\t\t------------------\n\n");
    printf("1 - Traiter les candidatures (output dans des fichiers)\n");
    printf("\n\n9 - Repasser en mode Utilisateur");
    printf("\n\n10 - Quitter\n");
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d", &choix);
    return choix;
}

int gestionResponsable(VilleIUT **tiut, int nbEle, Candidat **tcandid, int tailleL)
{
    int choix, codeRet, nb;
    char dept[30];
    char ville[30];
    char nom[30];
    Candidat** tabCandidV;
    printf("Nom du département concernée : ");
    scanf("%s", dept);
    printf("\nNom de la ville concernée : ");
    fgets(ville, 30, stdin);
    ville[strlen(ville) - 1] = '\0';
    printf("\nNom du responsable du département : ");
    fgets(nom, 30, stdin);
    nom[strlen(nom) - 1] = '\0';
    printf("\n");
    codeRet = verifChefDepart(tiut, nbEle, dept, ville, nom);
    if (codeRet == 0)
        return 0;
    while (1)
    {
        choix = menuResponsableAffiche();
        system("clear");
        switch (choix)
        {
        case 1:
            tabCandidV = candidDept(tcandid, dept, ville, tailleL,&nb);
            examinerCandid(tabCandidV, nb, dept, ville);
            system("clear");
            break;
        case 9:
            return 0;
        case 10:
            return -1;
        }
    }
}

/*
int longueur(ListeDept l)
{
    int cpt = 0;
    while (l != NULL)
    {
        cpt + 1;
        l = l->suiv;
    }
    return cpt;
}
*/

// ListeDept rechercheDept(ListeDept l, char code[])
// {
//     if (l == NULL)
//         return NULL;
//     if ((strcmp(code, l->departement)) < 0)
//         return NULL;
//     if (strcmp(code, l->departement) == 0)
//         return l;
//     return rechercheDept(l->suiv, code);
// }

// void SauvegarderIUT(VilleIUT **tabV, int tailleL)
// {
//     int i, nbDept;
//     FILE *flot;
//     flot = fopen("res.txt", "w");
//     for (i = 0; i < tailleL; i++)
//     {
//         fprintf(flot, "%s\n", tabV[i]->ville);
//         nbDept = longueurListe(tabV[i]->idDept);
//         fprintf(flot, "%d\n", nbDept);
//         SauvegarderListe(tabV[i]->idDept, flot);
//         free(tabV[i]);
//     }
// }

// void SauvegarderListe(ListeDept l, FILE *flot)
// {
//     MaillonDept *tmp;
//     while (l->suiv != NULL)
//     {
//         tmp = l;
//         fprintf(flot, "%s %d %s\n", l->departement, l->nbPlaces, l->nom);
//         l = l->suiv;
//         free(tmp);
//     }
//     fprintf(flot, "%s %d %s\n", l->departement, l->nbPlaces, l->nom);
//     free(l);
// }

// int longueurListe(ListeDept l)
// {
//     int cpt = 0;
//     while (l != NULL)
//     {
//         cpt += 1;
//         l = l->suiv;
//     }
//     return cpt;
// }

int traiterCandidIUTDept(Candidat *candid, char *dept, char *ville)
{
    int i, verif = 0, nb = 0;
    ListeCandid l = candid->idCandIUT;
    while (l != NULL)
    {
        if (strcmp(l->iutCandid, ville) == 0)
        {
            verif = 1;
            break;
        }
        l = l->suiv;
    }
    if (verif == 1)
    {
        for (i = 0; i < l->nbChoix; i++)
        {
            if (strcmp(l->tabDept[i]->departement, dept) == 0)
                return 1;
        }
    }
    return 0;
}

Candidat** candidDept(Candidat** tabCandidat, char* dept, char* ville, int tailleL, int* nb)
{
    int tphys = 50, i, codeRetour;
    Candidat **tabCandDept;
    tabCandDept = (Candidat **)malloc(sizeof(Candidat *) * tphys);
    if (tabCandDept == NULL)
    {
        printf("Pb malloc tab candid\n");
        exit(1);
    }
    *nb = 0;
    for (i = 0; i < tailleL; i++)
    {
        codeRetour = traiterCandidIUTDept(tabCandidat[i], dept, ville);
        if (codeRetour == 1)
        {
            tabCandDept[*nb] = tabCandidat[i];
            *nb += 1;
        }
    }
    return tabCandDept;
}

void SauvegardeCandidAdmis(Candidat **tab, int nb, int admis)
{
    FILE *flot;
    int i;
    float moy;
    if (admis == 1)
        flot = fopen("candidAdmis.txt", "w");
    else
        flot = fopen("candidMEA.txt", "w");
    if (flot == NULL)
    {
        printf("Problème ouverture fichier candidat admis\n");
        exit(1);
    }
    for (i = 0; i < nb; i++)
    {
        moy = (tab[i]->note[0] + tab[i]->note[1] + tab[i]->note[2] + tab[i]->note[3]) / 4;
        fprintf(flot, "%s %s %.1f\n", tab[i]->nom, tab[i]->prenom, moy);
    }
    fclose(flot);
}

void examinerCandid(Candidat **tabCandid, int nb, char* dept, char* ville)
{
    int nbAdmis, i, comptAdmis = 0, j, comptAttente = 0;
    float mini, moy;
    Candidat *tabAdmis[50];
    Candidat *tabEnAttente[50];
    ListeCandid l;
    printf("Nombre d'admis ?\n");
    scanf("%d", &nbAdmis);
    printf("Note minimum ?\n");
    scanf("%f", &mini);
    for (i = 0; i < nb; i++)
    {
        moy = (tabCandid[i]->note[0] + tabCandid[i]->note[1] + tabCandid[i]->note[2] + tabCandid[i]->note[3]) / 4;
        l = tabCandid[i]->idCandIUT;
        while (l != NULL)
        {
            if (strcmp(l->iutCandid, ville) == 0)
                break;
            l = l->suiv;
        }
        for (j = 0; j < l->nbChoix; j++)
        {
            if (strcmp(l->tabDept[j]->departement, dept) == 0)
            {
                if (moy >= mini)
                {
                    if (comptAdmis < nbAdmis)
                    {
                        l->tabDept[j]->decisionDept = 1;
                        tabAdmis[comptAdmis] = tabCandid[i];
                        comptAdmis += 1;
                    }
                    else
                    {
                        l->tabDept[j]->decisionDept = 2;
                        tabEnAttente[comptAttente] = tabCandid[i];
                        comptAttente += 1;
                    }
                }
                else
                    l->tabDept[j]->decisionDept = -1;
            }
        }
    }
    SauvegardeCandidAdmis(tabAdmis, comptAdmis, 1);
    SauvegardeCandidAdmis(tabEnAttente, comptAttente, 0);
}

/*
ListeDept ajouterEnQueue(ListeDept l, int x)
{
    if (l == NULL)
        return ajouterEnTête(l, x);
    l->suiv = ajouterEnQueue(l->suiv, x);
    return l;
}
*/