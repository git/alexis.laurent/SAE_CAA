#include "SAE.h"

bool motdepasseVerif(void)
{
    char mdp[10];
    int i=4;
    while(i > 0)
    {
        printf("entrer le mot de passe administrateur : ");
        system("stty -echo");
        scanf("%s",mdp);
        system("stty echo");
        if (strcmp(mdp,"123456") == 0)
        {
            system("clear");
            return true;
        }
        system("clear");
        printf("\n! mots de passe faux !\n");
        i = i - 1;
        printf("\n! il vous reste %d tentative/s !\n",i);
    }
    printf("Vous avez effectuer trop de tentative retour au menu Utilisateur !\n");
    return false;
}

bool verifQuit(void)
{
    char choix[4];
    while(1)
    {   
        system("clear");
        printf("Êtes-vous sûr de vouloir quitter ? (oui|non) : ");
        scanf("%s",choix);
        if (strcmp(choix,"oui")==0)
            return true;
        if (strcmp(choix,"non")==0)
            return false;  
        printf("Erreur de sélection ! Veuillez réessayer \n");
    }
}

bool verifSelection(void)
{
    char choix[4];
    while(1)
    {   
        printf("Voulez-vous continué la sélection (oui|non) : ");
        scanf("%s%*c",choix);
        if (strcmp(choix,"oui")==0)
            return true;
        if (strcmp(choix,"non")==0)
            return false;
        printf("Erreur de sélection ! Veuillez réessayer \n");
    }
}

bool confirmationSup(void)
{
    char choix[4];
    while(1)
    {
        printf("Êtes-vous sûr de vouloir supprimer/moddifié : ");
        scanf("%s", choix);
        if (strcmp(choix,"oui")==0)
            return true;
        if (strcmp(choix,"non")==0)
            return false;
        printf("Erreur de sélection ! Veuillez réessayer \n");
    }

}

int menuUtilisateurAffiche(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Menu Utilisateur|\n");
    printf("\t\t------------------\n\n");
    printf("1 - Consultation des IUT\n");
    printf("2 - Consultation des département global par IUT\n");
    printf("3 - Consultation du nombre de places pour un Département par IUT\n");
    printf("4 - Consultation du nombre de places pour un Département dans un IUT\n");
    printf("5 - Consultation des départements dans un IUT\n");
    printf("6 - Consultation de tout les IUT ayant un département choisi\n");
    printf("7 - Consultation de toute les informations des Département par IUT\n");
    printf("8 - Mode Candidat\n");
    printf("11 - Mode Responsable\n");
    printf("\n\n9 - Connexion mode Administrateur\n"); 
    printf("\n\n10 - Quitter\n");
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d",&choix);
    return choix;
}

int menuAdminAffiche(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Menu Administrateur|\n\n");
    printf("\t\t---------------------\n\n");
    printf("1 - Mise à jour nombre de Places d'un Département d'un IUT\n");
    printf("2 - Création d'un département dans un IUT\n");
    printf("3 - Suppression d'un département dans un IUT\n");
    printf("4 - Changement d'un responsable de département\n");
    printf("\n\n9 - Repasser en mode Utilisateur");
    printf("\n\n10 - Quitter\n");
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d",&choix);
    return choix;
}

void gestionMenu(VilleIUT **tiut, int nbEle, Candidat **tcandid, int *nbCandid, int *taillePCandid)
{
    int choix;
    while(1)
    {
        system("clear");
        choix = menuUtilisateurAffiche();
        system("clear");
        switch(choix) {
            case 1:
                AffichageVille(tiut,nbEle);
                clearpage();
                break;
            case 2:
                AffichageGlobalDepVille(tiut, nbEle);
                clearpage();
                break;
            case 3:
                AffichageNbplaceDept(tiut, nbEle);
                clearpage();
                break;
            case 4:
                AffichageNbplaceDeptVille(tiut, nbEle);
                clearpage();
                break;
            case 5:
                AffichageDepVille(tiut, nbEle);
                clearpage();
                break;
            case 6:
                AffichageDeptVille(tiut, nbEle);
                clearpage();
                break;
            case 7:
                AffichageGlobalIUT(tiut, nbEle);
                clearpage();
                break;
            case 8:
                gestionCandid(tiut, nbEle, tcandid, nbCandid, taillePCandid);
                break;
            case 9:
                choix = gestionMenuAdmin(tiut, nbEle);
                if (choix == -1)
                    if (verifQuit()) 
                    {
                        SauvegarderIUT(tiut, nbEle);
                        SauvegardeCandid(tcandid, *nbCandid);
                        return;
                    }
                break;
            case 10:
                if (verifQuit()) 
                {
                    SauvegarderIUT(tiut, nbEle);
                    SauvegardeCandid(tcandid, *nbCandid);
                    return;
                }
                break;
            case 11:
                choix = gestionResponsable(tiut, nbEle, tcandid, *nbCandid);
                if (choix == -1)
                    if(verifQuit())
                    {
                        SauvegarderIUT(tiut, nbEle);
                        SauvegardeCandid(tcandid, *nbCandid);
                        return;
                    }
                    break;
                break;
        }

    }
}

int gestionMenuAdmin(VilleIUT **tiut, int nbEle)
{
    int choix;
    if (!motdepasseVerif())
        return 0;
    while(1)
    {
        choix = menuAdminAffiche();
        system("clear");
        switch(choix){
            case 1:
                MiseAJourNbPlace(tiut, nbEle);
                system("clear");
                break;
            case 2:
                creationDept(tiut, nbEle);
                system("clear");
                break;
            case 3:
                suppressionDept(tiut, nbEle);
                system("clear");
                break;
            case 4:
                modifChefDept(tiut, nbEle);
                system("clear");
                break;
            case 9:
                return 0;
            case 10:
                return -1;
        }
    }
}

void global(void)
{
    int tailleL, i, tphys, nb;
    VilleIUT** tab;
    Candidat **tabC;
    char nomFich[20];
    strcpy(nomFich,"iut.txt");
    printf("charg\n");
    tab = chargementVille(nomFich,30,&tailleL);
    tabC = chargmentCandid("candidature.txt", &tphys, &nb);
    system("clear");
    printf("\n");
    gestionMenu(tab, tailleL, tabC, &nb, &tphys);
}

void AffichageVille(VilleIUT **tiut, int nbEle)
{
    int i;
    printf("\tIUT par ville :\n");
    for(i=0;i<nbEle;i++)
    {
        printf("- %s\n\t",tiut[i]->ville);
    }
}

void AffichageGlobalDepVille(VilleIUT **tiut, int nbEle)
{
    int i;
    MaillonDept *aux;
    printf("\tDépartement par IUT :\n");
    for(i=0;i<nbEle;i++)
    {
        printf("%s :\n", tiut[i]->ville);
        aux = tiut[i]->idDept;
        while(aux != NULL)
        {
            printf("|%s|\n",aux->departement);
            aux = aux->suiv;
        }
    }
}

void AffichageNbplaceDept(VilleIUT **tiut, int nbEle)
{
    int pos;
    MaillonDept *aux;
    for (pos = 0; pos < nbEle; pos++)
    {   
        printf("%s -> \t", tiut[pos]->ville);
        aux = tiut[pos]->idDept;
        while(aux->suiv != NULL && pos < nbEle)
        {
            printf("|%s, %d places|\n\t",aux->departement, aux->nbPers);
            aux = aux->suiv;
        }
        printf("\n");
    }
}

int rechVille(VilleIUT **tiut, int nbEle, char code[], int *trouve)
{
    int inf = 0, sup = nbEle - 1, m;
    while(inf <= sup)
    {
        m = (inf + sup)/2;
        if (strcmp(tiut[m]->ville, code)==0)
        {
            *trouve = 1;
            return m;
        }
        if (strcmp(tiut[m]->ville, code)>0)
            sup = m - 1;
        else inf = m + 1;
    }
    *trouve = 0;
    return inf;
}

void AffichageNbplaceDeptVille(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    char code[30];
    int pos,trouve = 0;
    while(1)
    {
        printf("IUT : ");
        scanf("%s",code);
        pos = rechVille(tiut, nbEle, code,&trouve);
        if (trouve == 1)

        {
            while(1)
            {
                printf("Département : ");
                scanf("%s",code);
                trouve = 0;
                aux = rechercheDept(tiut[pos]->idDept, code);
                if (aux == NULL)
                    printf("Département non présent à l'iut de %s",tiut[pos]->ville);
                if (aux != NULL)
                    printf("%s -> |%s, %d places|\n",tiut[pos]->ville,aux->departement, aux->nbPers);
                printf("Saisi Département :\n");
                if(!verifSelection())
                    break;
            }
        }
        if (trouve == 0) printf("%s n'a pas d'IUT\n",code);
        printf("Saisi Département :\n");
        if(!verifSelection())
            break;
    }
}

void AffichageDepVille(VilleIUT **tiut, int nbEle)
{
    char code[30];
    int pos,trouve = 0;
    while(1)
    {
        printf("IUT : ");
        scanf("%s",code);
        pos = rechVille(tiut, nbEle, code,&trouve);
        if (trouve == 1)
        {
            printf("%s :\n\t",tiut[pos]->ville);
            affichageListeDept(tiut[pos]->idDept);
        }
        if (trouve == 0)
            printf("%s n'a pas d'IUT\n",code);
        printf("Saisi Ville :\n");
        if(!verifSelection())
            break;
    }
}

void AffichageDeptVille(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    char code[30];
    int pos = 0, nbocc = 0;
    while(1)
    {
        printf("département : ");
        scanf("%s",code);
        printf("\nIUT ayant un département %s : \n",code);
        for(pos = 0; pos < nbEle; pos++)
        {
            aux = rechercheDept(tiut[pos]->idDept, code);
            if(aux != NULL)
            {
                printf("- %s\n", tiut[pos]->ville);
                nbocc = nbocc + 1;
            }
        }
        if (nbocc == 0)
            printf("aucun département %s est présent en France\n", code);
        printf("Saisi Département :\n");
        if(!verifSelection())
            break;
    }
}

void AffichageGlobalIUT(VilleIUT **tiut, int nbEle)
{
    int pos = 0;
    printf("Ville : \t");
    while(pos < nbEle)
    {   
        printf("-%s \n", tiut[pos]->ville);
        printf("Département:\tNombre de places:\tNom du Responsable:\n");
        affichageListeDept(tiut[pos]->idDept);
        pos = pos + 1;
        printf("\n");
    }
}

void MiseAJourNbPlace(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    int nouvNbPlace, pos, trouve;
    char code[30];
    while(1)
    {   
        printf("IUT : ");
        scanf("%s",code);        
        pos = rechVille(tiut, nbEle, code,&trouve);
        if (trouve == 1)
        {
            while(1)
            {
                printf("Département : ");
                scanf("%s",code);
                aux = rechercheDept(tiut[pos]->idDept, code);
                if (aux != NULL)
                {
                    printf("Ancien nombre de places : %d\n",aux->nbPers);
                    printf("Nouveau nombre de places : ");
                    scanf("%d", &nouvNbPlace);
                    aux->nbPers = nouvNbPlace;
                    printf("Mise à jour effectuer avec succès !");
                    break;
                }
                if (aux == NULL)
                    printf("Département non présent à l'iut de %s",tiut[pos]->ville);
                printf("Saisi Département :\n");
                if(!verifSelection())
                    break;
            }
        }
        if (trouve == 0) 
            printf("Cet ville n'a pas d'IUT\n");
        printf("Saisi Ville :\n");
        if(!verifSelection())
            break;
    }
}


void creationDept(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    char code[30], nom[30];
    int trouve, pos, nbP;
    while(1)
    {
        printf("Dans quel IUT voulez-vous créer un département : ");
        scanf("%s", code);
        pos = rechVille(tiut, nbEle, code, &trouve);
        if (trouve == 1)
        {
            while(1)
            {
                printf("Nom du Département : ");
                scanf("%s%*c", code);
                aux = rechercheDept(tiut[pos]->idDept, code);
                if (aux != NULL)
                    printf("Département déjà existant\n");
                if (aux == NULL)
                {
                    printf("Nom du Responsable du Département %s : ",code);
                    fgets(nom,30,stdin);
                    nom[strlen(nom)-1] = '\0';
                    printf("%s\n", nom);
                    printf("Nombre de place du Département %s : ",code);
                    scanf("%d", &nbP);
                    tiut[pos]->idDept = insererDept(tiut[pos]->idDept,code,nom,nbP);
                    printf("insertion du département %s effectuer \n", code);
                }
                printf("Saisi Département :\n");
                if (!verifSelection())
                    break;
            }
        }
        if (trouve == 0) 
            printf("Cet ville n'a pas d'IUT\n");
        printf("Saisi Ville :\n");
        if(!verifSelection())
            break;

    }

}

void suppressionDept(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    char code[30];
    int trouve, pos;
    while(1)
    {
        printf("Dans quel IUT voulez-vous supprimer un département : ");
        scanf("%s", code);
        pos = rechVille(tiut, nbEle, code, &trouve);
        if (trouve == 1)
        {
            printf("Nom du Département : ");
            scanf("%s", code);
            aux = rechercheDept(tiut[pos]->idDept, code);
            if (aux != NULL)
                if (confirmationSup())
                    tiut[pos]->idDept = supprimerDept(tiut[pos]->idDept, code);
            if (aux == NULL)
                printf("Ce Département n'existe pas dans l'IUT de %s \n",tiut[pos]->ville);
            printf("Saisi Département :\n");
            if (!verifSelection())
                break;       
        }
        if (trouve == 0) 
            printf("Cet ville n'a pas d'IUT\n");
        printf("Saisi Ville :\n");
        if (!verifSelection())
            break; 

    }
}

void modifChefDept(VilleIUT **tiut, int nbEle)
{
    MaillonDept *aux;
    char code[30];
    int trouve, pos;
    while(1)
    {
        printf("Dans quel IUT voulez-vous modifier le chef de département : ");
        scanf("%s", code);
        pos = rechVille(tiut, nbEle, code, &trouve);
        if (trouve == 1)
        {
            printf("Nom du Département : ");
            scanf("%s%*c", code);
            aux = rechercheDept(tiut[pos]->idDept, code);
            if (aux != NULL)
            {   
                printf("Nom de l'ancien chef de département %s de %s : %s\n",aux->departement, tiut[pos]->ville,aux->responsable);
                printf("Nom du nouveau chef de département %s de %s :",aux->departement, tiut[pos]->ville);
                fgets(code,30,stdin);
                code[strlen(code)-1] = '\0';
                strcpy(aux->responsable, code);
                printf("Chef de département : %s\n",aux->responsable);
            }
            if (aux == NULL)
                printf("Ce Département n'existe pas dans l'IUT de %s \n",tiut[pos]->ville);
            printf("Saisi Département :\n");
            if (!verifSelection())
                break;   
        }
        if (trouve == 0) 
            printf("Cet ville n'a pas d'IUT\n");
        printf("Saisi Ville :\n");
        if (!verifSelection())
            break;  

    }
}

void triEchange(VilleIUT **tiut, int nbEle)
{
    int pge;
    while(nbEle>1)
    {
        pge = plusGrand(tiut, nbEle);
        echanger(tiut,pge,nbEle-1);
        nbEle = nbEle - 1;
    }
}

int plusGrand(VilleIUT **tiut, int nbEle)
{
    int pge = 0, i;
    for (i = 1; i < nbEle; i++)
        if (strcmp(tiut[i]->ville,tiut[pge]->ville)>0)
            pge = i;
    return pge;
}

void echanger(VilleIUT **tiut, int i, int j)
{
    VilleIUT *aux;
    aux = tiut[i];
    tiut[i] = tiut[j];
    tiut[j] = aux;
}


void clearpage(void)
{
    char entre;
    printf("\nappuyé sur la touche [ENTREE] pour continuer");
    scanf("%*c%c%*c", &entre);
    system("clear");
}

ListeCandid ListeCandidvide(void)
{
    return NULL;
}

ListeCandid insererEnTeteCandid(ListeCandid l, char *ville, ChoixDept **tDept, int nbEle)
{
    MaillonCandid *m;
    m = (MaillonCandid *)malloc(sizeof(MaillonCandid));
    if (m == NULL)
    {
        printf("Pb maillon");
        exit(1);
    }
    strcpy(m->iutCandid, ville);
    m->nbChoix = nbEle;
    m->tabDept = tDept;
    m->suiv = l;
    return m;
}

ListeCandid insererCandid(ListeCandid l, char *ville, ChoixDept **tDept, int nbEle)
{
    if (l == NULL)
        return insererEnTeteCandid(l, ville, tDept, nbEle);
    if (strcmp(ville, l->iutCandid) < 0)
        return insererEnTeteCandid(l, ville, tDept, nbEle);
    if (strcmp(ville, l->iutCandid) == 0)
        return l;
    l->suiv = insererCandid(l->suiv, ville, tDept, nbEle);
    return l;
}

int rechCandiDept(ChoixDept **tDept, int tailleL, char dept[], int *trouve)
{
    int inf = 0, sup = tailleL + 1, m;
    while(inf <= sup)
    {
        m = (inf + sup)/2;
        if (strcmp(tDept[m]->departement, dept)==0)
        {
            *trouve = 1;
            return m;
        }
        if (strcmp(tDept[m]->departement, dept)<0)
            sup = m-1;
        else inf = m+1;
    }
    *trouve = 0;
    return inf;
}

int plusGrandCandidDept(ChoixDept **tDept, int tailleL)
{
    int pge = 0, i;
    for (i = 1; i < tailleL; i++)
    {
        if(strcmp(tDept[i]->departement, tDept[pge]->departement)>0)
            pge = i;
        return pge;
    }
}

void echangerCandidDept(ChoixDept **tDept, int i, int j)
{
    ChoixDept *aux;
    aux = tDept[i];
    tDept[i] = tDept[j];
    tDept[j] = aux;
}

void triEchangeCandidDept(ChoixDept **tDept, int tailleL)
{
    int pge;
    while(tailleL > 1)
    {
        pge = plusGrandCandidDept(tDept, tailleL);
        echangerCandidDept(tDept, pge, tailleL - 1);
        tailleL = tailleL - 1;
    }
}

void videTabDept(ChoixDept **tDept, int tailleP)
{
    ChoixDept *aux;
    int i;
    if (tailleP == 0)
        free(tDept);
    for (i=tailleP - 1; i >= 0; i--)
    {
        aux = tDept[i];
        free(aux);
    }
}

ListeCandid suppressionEnTeteCandid(ListeCandid l , ChoixDept **tDept, int tailleP)
{
    MaillonCandid *aux;
    if(l == NULL)
    {
        printf("opération interdite !\n");
        exit(1);
    }
    aux = l;
    videTabDept(tDept, tailleP);
    l = l->suiv;
    free(aux);
    return l;
}

ListeCandid suppressionCandid(ListeCandid l, char *ville, ChoixDept **tDept, int tailleP)
{
    if (l == NULL)
        return l;
    if (strcmp(ville, l->iutCandid) < 0)
        return l;
    if (strcmp(ville, l->iutCandid) == 0)
        return suppressionEnTeteCandid(l, tDept, tailleP);
    l->suiv = suppressionCandid(l->suiv, ville, tDept, tailleP);
    return l;
}

void afficherCandid(Candidat **tab, int nbEle)
{
    int i;
    for (i=0; i < nbEle; i++)
    {
        printf("%d %s %s |%.1f %.1f %.1f %.1f|\n", tab[i]->id, tab[i]->nom, tab[i]->prenom, tab[i]->note[0], tab[i]->note[1], tab[i]->note[2], tab[i]->note[3]);
        afficherCandidIUT(tab[i]->idCandIUT);
        printf("\n");
    }
    printf("\n");
}

void afficherCandidIUT(ListeCandid l)
{
    if (l == NULL)
        return;
    printf("%s : %d\n", l->iutCandid,l->nbChoix);
    afficherCandidDept(l->tabDept, l->nbChoix);
    l=l->suiv;
    return afficherCandidIUT(l);
}

void afficherCandidDept(ChoixDept **tab, int nbChoix)
{
    int i;
    for (i=0; i < nbChoix; i++)
    {
        printf("|%s %d %d|\n", tab[i]->departement, tab[i]->decisionDept, tab[i]->decisionCandid);
    }
}
int longueur(ListeCandid l)
{
    if(l==NULL)
        return 0;
    return 1 + longueur(l->suiv);
}

Candidat **chargmentCandid(char *nomFich, int *tphys, int *tailleL)
{
    int i = 0, j = 0, nbIUT, nbDept;
    FILE *flot;
    Candidat **tcandid;
    flot = fopen(nomFich, "r");
    if (flot == NULL)
    {
        printf("Problèmes d'ouverture de fichier !\n");
        exit(1);
    }
    fscanf(flot,"%d",tphys);
    *tphys = *tphys + 10;
    tcandid = (Candidat **)malloc(sizeof(Candidat *) * *tphys);
    if (tcandid == NULL)
    {
        printf("Erreur malloc tcandid !\n");
        exit(1);
    }
    *tailleL = 0;
    tcandid[*tailleL] = (Candidat *)malloc(sizeof(Candidat));
    if (tcandid[*tailleL] == NULL)
    {
        printf("Erreur malloc candidat !\n");
        fclose(flot);
        exit(1);
    }
    *(tcandid[*tailleL]) = lireCandidat(flot);
    while(!feof(flot))
    {
        fscanf(flot, "%d", &nbIUT);
        printf("%d\n", nbIUT);
        tcandid[*tailleL]->idCandIUT = ListeCandidvide();
        while(i < nbIUT)
        {
            tcandid[*tailleL]->idCandIUT = traiterCandidIUT(tcandid[*tailleL]->idCandIUT, flot);
            i = i + 1;
        }
        i = 0;
        *tailleL = *tailleL + 1;
        tcandid[*tailleL] = (Candidat *)malloc(sizeof(Candidat));
        if (tcandid[*tailleL] == NULL)
        {
            printf("Erreur malloc candidat !\n");
            fclose(flot);
            exit(1);
        }
        printf("\n\n");
        *(tcandid[*tailleL]) = lireCandidat(flot);
    }
    if (tailleL == 0)
        printf("Fichier vide !\n");
    fclose(flot);
    return tcandid;
}

Candidat lireCandidat(FILE *flot)
{
    Candidat c;
    fscanf(flot,"%d",&c.id);
    fscanf(flot,"%s%*c",c.nom);
    fgets(c.prenom,30,flot);
    c.prenom[strlen(c.prenom) - 1] = '\0';
    fscanf(flot, "%f %f %f %f", &c.note[0], &c.note[1], &c.note[2], &c.note[3]);
    printf("%d %s %s %.1f %.1f %.1f %.1f\n", c.id, c.nom, c.prenom, c.note[0], c.note[1], c.note[2], c.note[3]);
    return c;
}

ListeCandid traiterCandidIUT(ListeCandid l, FILE *flot)
{
    char ville[30];
    int nbDept, i=0;
    ChoixDept **tDept;
    fscanf(flot,"%s",ville);
    fscanf(flot, "%d", &nbDept);
    printf("%s |%d|\n", ville, nbDept);
    tDept = (ChoixDept **)malloc(sizeof(ChoixDept *)*(nbDept + 10));
    if(tDept == NULL)
    {
        printf("Pb malloc candidIUt\n");
        exit(1);
    }
    while (i < nbDept)
    {
        tDept[i] = traiterCandidDept(i, nbDept, flot);
        i = i + 1;
    }
    l = insererCandid(l , ville , tDept, nbDept);
    return l;
}

ChoixDept *traiterCandidDept(int tailleL, int tailleP, FILE *flot)
{
    ChoixDept *cD;
    char dept[30];
    int deciDept, deciCandid;
    cD = (ChoixDept *)malloc(sizeof(ChoixDept));
    if (cD == NULL)
    {
        printf("Pb malloc");
        exit(1);
    }
    fscanf(flot,"%s", dept);
    fscanf(flot,"%d", &deciDept);
    fscanf(flot,"%d",&deciCandid);
    strcpy(cD->departement, dept);
    cD->decisionDept = deciDept;
    cD->decisionCandid = deciCandid;
    printf("|%s %d %d|\n", cD->departement, cD->decisionDept, cD->decisionCandid);
    return cD;
}

int rechCandid(Candidat **tab, int nbEle, int id, int *trouve)
{
    int i;
    for (i=0; i < nbEle; i++)
    {
        if (tab[i]->id == id)
        {
            *trouve = 1;
            return i;
        }
        if (tab[i]->id > id)
        {
            *trouve = 0;
            return i;
        }
    }
    *trouve = 0;
    return nbEle - 1;
}

void triEchangecandid(Candidat **tab, int nbEle)
{
    int pge;
    while(nbEle > 1)
    {
        pge = plusGrandCandid(tab, nbEle);
        echangerCandid(tab, pge, nbEle - 1);
        nbEle = nbEle - 1;
    }
}

int plusGrandCandid(Candidat **tab, int nbEle)
{
    int pge = 0, i;
    for (i = 1; i < nbEle; i++)
    {
        if(tab[i]->id > tab[pge]->id)
        {
            pge = i;
        }
        return pge;
    }
}

void echangerCandid(Candidat **tab, int i, int j)
{
    Candidat *aux;
    aux = tab[i];
    tab[i] = tab[j];
    tab[j] = aux;
}

ListeCandid rechCandidIUT(ListeCandid l, char *code)
{
    if (l == NULL)
        return NULL;
    if ((strcmp(code, l->iutCandid)) < 0)
        return NULL;
    if ((strcmp(code, l->iutCandid)) == 0)
        return l;
    return rechCandidIUT(l->suiv, code);
}

void decalageGauche(Candidat **tab,int pos, int nbEle)
{
    int i;
    for(i = pos; i < nbEle - 1; i++)
        tab[i] = tab[i + 1];
}

void supprimerCandid(Candidat **tab, int *nbEle)
{
    int idSup, pos, trouve;
    while(1)
    {
        printf("Identifiant de la candidiature à supprimer : ");
        scanf("%d", &idSup);
        pos = rechCandid(tab, *nbEle, idSup, &trouve);
        if (trouve == 0)
        {
            printf("Candidature non présente dans la base de donnée\n");
            break;
        }
        if (trouve == 1)
            if (!confirmationSup())
                return;
            while(tab[pos]->idCandIUT != NULL)
            {
                suppressionCandid(tab[pos]->idCandIUT, tab[pos]->idCandIUT->iutCandid, tab[pos]->idCandIUT->tabDept, tab[pos]->idCandIUT->nbChoix);
                tab[pos]->idCandIUT = tab[pos]->idCandIUT->suiv;
            }
            free(tab[pos]);
            decalageGauche(tab, pos, *nbEle);
            *nbEle = *nbEle - 1;
            return;
            
        if(!verifSelection())
            return;
    }
    return;
}

void decalagenom(char* tab, int pos, int nbEle)
{
    int i;
    for(i = pos; i < nbEle - 1; i++)
        tab[i] = tab[i + 1];
}

Candidat **reallocTCandid(Candidat **tab, int *tailleP)
{
    Candidat **aux;
    *tailleP = *tailleP + 1;
    aux = (Candidat **)realloc(tab, *tailleP * sizeof(Candidat *));
    if (aux == NULL)
    {
        printf("Erreur realloc\n");
        exit(1);
    }
    return aux;
}

void creationCandid(VilleIUT **tiut, int nbIUT, Candidat **tcandid, int *nbCandid, int *taillePCandid)
{
    char nom[30], prenom[30], choix[4];
    int idAdd, i, iCandid;
    Candidat c;
    if (*nbCandid == *taillePCandid)
        tcandid = reallocTCandid(tcandid, taillePCandid);
    while(1)
    {
        idAdd = tcandid[*nbCandid - 1]->id + 1;
        printf("Votre nom : ");
        scanf("%s%*c", prenom);
        printf("Votre prénom : ");
        fgets(nom,30, stdin);
        nom[strlen(nom) - 1] = '\0';
        printf("%s %s\n", prenom, nom);
        printf("vos 4 notes : ");
        scanf("%f %f %f %f", &c.note[0], &c.note[1], &c.note[2], &c.note[3]); 
        printf("note : |%.1f|%.1f|%.1f|%.1f|\n",c.note[0], c.note[1], c.note[2], c.note[3]);
        while(1)
        {
            printf("vous valider votre candidature (oui|non) : ");
            scanf("%s", choix);
            if (strcmp(choix, "oui")==0)
            {
                c.id = idAdd;
                strcpy(c.nom, nom);
                strcpy(c.prenom, prenom);
                c.idCandIUT = ListeCandidvide();
                tcandid[*nbCandid] = (Candidat *)malloc(sizeof(Candidat));
                *nbCandid = *nbCandid + 1;
                *(tcandid[*nbCandid - 1]) = c;
                printf("création terminer \n");
                break;
            }
            if (strcmp(choix, "non")==0)
                return;
        }
        while(1)
        {
            printf("voulez vous faire votre choix maintenant (oui|non) : ");
            scanf("%s", choix);
            if (strcmp(choix, "oui")==0)
                modifCandid(tcandid, *nbCandid, tiut, nbIUT, *nbCandid - 1);
            if (strcmp(choix, "non")==0)
                break;
            return;
        }
        if(!verifSelection())
            break;
    }
    return;
}

int menuCandid(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Menu Candidature|\n");
    printf("\t\t------------------\n\n");
    printf("1 - Créer une Candidature\n");
    printf("2 - Supprimer une Candidature\n");
    printf("3 - Modifier une Candidature\n");
    printf("4 - Afficher une Candidature\n");
    printf("5 - Afficher toute les Candidatures\n");
    printf("6 - Afficher Admission\n");
    printf("7 - Gérer les Admission\n");
    printf("\n\n9 - Connexion mode Administrateur\n"); 
    printf("\n\n10 - Quitter\n");
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d",&choix);
    return choix;
}

void afficher1Candid(Candidat **tab, int nbCandid)
{
    int i, trouve, id;
    printf("identifiant : ");
    scanf("%d", &id);
    i = rechCandid(tab, nbCandid, id, &trouve);
    if(trouve == 0)
    {
        printf("Identifiant absent de la base de donnée\n");
        return;
    }
    printf("%d %s %s |%.1f %.1f %.1f %.1f|\n", tab[i]->id, tab[i]->nom, tab[i]->prenom, tab[i]->note[0], tab[i]->note[1], tab[i]->note[2], tab[i]->note[3]);
    afficherCandidIUT(tab[i]->idCandIUT);
    printf("\n");
}

void gestionCandid(VilleIUT **tiut, int nbIUT, Candidat **tcandid, int *nbCandid, int *taillePCandid)
{
    int choix, id, pos, trouve;
    while(1)
    {
        choix = menuCandid();
        system("clear");
        switch(choix) {
            case 1:
                creationCandid(tiut, nbIUT ,tcandid, nbCandid, taillePCandid);
                system("clear");
                break;
            case 2:
                supprimerCandid(tcandid, nbCandid);
                clearpage();
                break;
            case 3:
                printf("Votre identifiant : ");
                scanf("%d", &id);
                pos = rechCandid(tcandid, *nbCandid, id, &trouve);
                if (trouve == 0)
                    printf("identifiant pas présent dans la base de donnée !\n");
                else
                    modifCandid(tcandid, *nbCandid, tiut, nbIUT, pos);
                system("clear");
                break;
            case 4:
                afficher1Candid(tcandid, *nbCandid);
                clearpage();
                break;
            case 5:
                afficherCandid(tcandid, *nbCandid);
                clearpage();
                break;
            case 6:
                afficherAdmiCandid(tcandid, *nbCandid);
                clearpage();
                break;
            case 7:
                gererAdmiCandid(tcandid, *nbCandid);
                break;
            case 9:
                choix = gestionMenuAdmin(tiut, nbIUT);
                if (choix == -1)
                    return;
            case 10:
                return;
        }

    }
}

int menuGestionCandid(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Gestion Candidature|\n");
    printf("\t\t---------------------\n\n");
    printf("1 - Ajouter un voeux\n");
    printf("2 - Supprimer un voeux\n");
    printf("\n\n9 - Retour Menu Candidature\n"); 
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d",&choix);
    return choix;
}

ListeCandid recherchecandidIUT(ListeCandid l, char code[])
{
    if (l == NULL)
        return NULL;
    if ((strcmp(code,l->iutCandid)) < 0)
        return NULL;
    if (strcmp(code,l->iutCandid)==0)
        return l;
    return recherchecandidIUT(l->suiv, code);
}

int recherchecandidDept(ChoixDept **tab,int nbEle, char code[],int *trouve)
{
    int i=0;
    while(i < nbEle)
    {
        if ((strcmp(code,tab[i]->departement)) < 0)
        {
            *trouve = 0;
            return -1;
        }
        if (strcmp(code,tab[i]->departement)==0)
        {
            *trouve = 1;
            return i;
        }
        i = i + 1;
    }
    *trouve = 0;
    return -1;
    
}

ChoixDept RempTabCandid(char dept[],int deciCandid ,int deciDept)
{
    ChoixDept d;
    strcpy(d.departement, dept);
    d.decisionDept = deciDept;
    d.decisionCandid = deciCandid;
    return d;

}

void ajouterDept(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid)
{
    char ville[30], departement[30];
    int pos,irech, trouve;
    MaillonDept *auxD;
    MaillonCandid *aux;
    aux = (MaillonCandid *)malloc(sizeof(MaillonCandid));
    if (aux == NULL)
    {
        printf("Erreur Malloc ! \n");
        exit(1);
    }
    ChoixDept **tDept;
    while(1)
    {
        printf("Dans quel IUT voulez vous candidater : ");
        scanf("%s%*c", ville);
        pos = rechVille(tiut, nbIUT, ville, &trouve);
        if (trouve == 0)
            printf("Erreur ! IUT non référencé\n");
        if(trouve == 1)
        {
            while(1)
            {
                printf("Dans quel Département voulez-vous candidater : ");
                scanf("%s%*c", departement);
                auxD = rechercheDept(tiut[pos]->idDept, departement);
                if (auxD == NULL)
                    printf("Erreur ! Département non référencé\n");
                if (auxD != NULL)
                    {
                        printf("1\n");
                        aux = recherchecandidIUT(tcandid[iCandid]->idCandIUT, ville);
                        printf("2\n");
                        if (aux == NULL)
                        {
                            printf("3\n");
                            tDept = (ChoixDept **)malloc(10 * sizeof(ChoixDept *));
                            printf("4\n");
                            if(tDept == NULL)
                            {
                                printf("Pb malloc candidIUt\n");
                                exit(1);
                            }
                            printf("5\n");
                            tDept[0] = (ChoixDept *)malloc(sizeof(ChoixDept));
                            if (tDept[0] == NULL)
                            {
                                printf("pb malloc\n");
                                exit(1);
                            }
                            *(tDept[0]) = RempTabCandid(departement, 0, 0);
                            printf("%s %d %d\n",tDept[0]->departement, tDept[0]->decisionDept, tDept[0]->decisionCandid);
                            printf("6\n");
                            tcandid[iCandid]->idCandIUT = insererCandid(tcandid[iCandid]->idCandIUT, ville, tDept, 1);
                            printf("Candidature Validée \n");
                        }
                        if (aux != NULL)
                        {
                            printf("3\n");
                            irech = recherchecandidDept(aux->tabDept, aux->nbChoix, departement, &trouve);
                            printf("4\n");
                            if (irech == -1)
                            {
                                printf("5\n");
                                aux->tabDept[aux->nbChoix] = (ChoixDept *)malloc(sizeof(ChoixDept));
                                if (aux->tabDept[aux->nbChoix] == NULL)
                                {
                                    printf("Erreur Malloc \n");
                                    exit(1);
                                }
                                *(aux->tabDept[aux->nbChoix]) = RempTabCandid(departement, 0, 0);
                                printf("6\n");
                                aux->nbChoix = aux->nbChoix + 1;
                                strcpy(aux->iutCandid, ville);
                                printf("Candidature validée !\n");
                            }
                            else
                                printf("vous avez déjà postuler au département %s de cette %s", departement, ville);
                        }
                    }
                printf("saisie Département\n");
                if(!verifSelection())
                    break;
            }
        }
        printf("Saisie IUT\n");
        if(!verifSelection())
            break;
    }
    return;
}

void decalageGaucheDept(ChoixDept **tab, int pos, int nbEle)
{
    int i;
    for(i = pos; i < nbEle - 1; i++)
        tab[i] = tab[i + 1];
}

void supprimerCandidDept(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid)
{
    char ville[30], departement[30];
    int pos, trouve;
    MaillonCandid *aux;
    while(1)
    {
        printf("Dans quel IUT voulez-vous supprimer votre candidature : ");
        scanf("%s%*c", ville);
        aux = rechCandidIUT(tcandid[iCandid]->idCandIUT, ville); 
        if(aux== NULL)
            printf("Aucun voeux formuler dans cette IUT\n");
        else
            while(1)
            {
                printf("Dans quel Département voulez-vous supprimer votre candidature : ");
                scanf("%s%*c", departement);
                pos = recherchecandidDept(aux->tabDept, aux->nbChoix, departement, &trouve);
                if (trouve == 0)
                    printf("Aucun voeux formuler dans ce département de l'IUT\n");
                else
                    if(!confirmationSup())
                        printf("Annulation confirmé !\n");
                    else
                        free(aux->tabDept[pos]);
                        decalageGaucheDept(aux->tabDept, pos, aux->nbChoix);
                        aux->nbChoix = aux->nbChoix - 1;
                        if (aux->nbChoix == 0)
                            tcandid[iCandid]->idCandIUT = suppressionCandid(tcandid[iCandid]->idCandIUT, ville, aux->tabDept, aux->nbChoix);
                        printf("voeux supprimmé !\n");
                        break;
            }
            break;
    }

}

void modifCandid(Candidat **tcandid, int nbCandid, VilleIUT **tiut, int nbIUT, int iCandid)
{
    int choix;
    system("clear");
    while(1)
    {
        choix = menuGestionCandid();
        system("clear");
        switch(choix){
            case 1:
                ajouterDept(tcandid, nbCandid, tiut, nbIUT, iCandid);
                break;
            case 2:
                supprimerCandidDept(tcandid, nbCandid, tiut, nbIUT, iCandid);
                break;
            case 9:
                return;
        }
    }
}

ListeDept creerListeDept(void)
{
    return NULL;
}

VilleIUT **chargementVille(char *nomFich, int tphys, int *tailleL)
{
    int i = 0, nbDept;
    FILE *flot;
    VilleIUT **tabV;
    tabV = (VilleIUT **)malloc(sizeof(VilleIUT *) * tphys);
    if (tabV == NULL)
    {
        printf("Erreur malloc tab!\n");
        exit(1);
    }
    flot = fopen(nomFich, "r");
    if (flot == NULL)
    {
        printf("Problème ouverture du fichier !\n");
        exit(1);
    }
    *tailleL = 0;
    tabV[*tailleL] = (VilleIUT *)malloc(sizeof(VilleIUT));
    if (tabV[*tailleL] == NULL)
    {
        printf("Erreur malloc ville!\n");
        fclose(flot);
        exit(1);
    }
    *(tabV[*tailleL]) = lireVille(flot);
    while (!feof(flot))
    {
        fscanf(flot, "%d", &nbDept);
        printf("Nombre de dep : %d\n", nbDept);
        tabV[*tailleL]->idDept = creerListeDept();
        while (i < nbDept)
        {
            tabV[*tailleL]->idDept = traiterDept(tabV[*tailleL]->idDept, flot);
            i += 1;
        }
        i = 0;
        *tailleL += 1;
        tabV[*tailleL] = (VilleIUT *)malloc(sizeof(VilleIUT));
        if (tabV[*tailleL] == NULL)
        {
            printf("Erreur malloc ville!\n");
            fclose(flot);
            exit(1);
        }
        *(tabV[*tailleL]) = lireVille(flot);
    }
    if (tailleL == 0)
        printf("Fichier vide !\n");
    fclose(flot);
    return tabV;
}

VilleIUT lireVille(FILE *flot)
{
    VilleIUT v;
    fgets(v.ville, 30, flot);
    v.ville[strlen(v.ville) - 1] = '\0';
    return v;
}

ListeDept traiterDept(ListeDept l, FILE *flot)
{
    char nom[30], dept[30];
    int nbPlaces;
    fscanf(flot, "%s %d", dept, &nbPlaces);
    fgets(nom, 30, flot);
    decalagenom(nom,0,strlen(nom));
    nom[strlen(nom) - 2] = '\0';
    printf("Nom du départ : %s\n", dept);
    l = insererDept(l, dept, nom, nbPlaces);
    return l;
}

ListeDept ajouterEnTeteDept(ListeDept l, char *dept, char *nom, int nbP)
{
    MaillonDept *nouv;
    nouv = (MaillonDept *)malloc(sizeof(MaillonDept));
    if (nouv == NULL)
    {
        printf("Erreur malloc !\n");
        exit(1);
    }
    strcpy(nouv->departement, dept);
    nouv->nbPers = nbP;
    strcpy(nouv->responsable, nom);
    nouv->suiv = l;
    return nouv;
}

void affichageListeDept(ListeDept l) // itératif
{
    while (l != NULL)
    {
        printf("- %s :\t|%d places\t\t%s|\n", l->departement, l->nbPers, l->responsable);
        l = l->suiv;
    }
}

ListeDept insererDept(ListeDept l, char *dept, char *nom, int nbP)
{ // récursif
    if (l == NULL)
        return ajouterEnTeteDept(l, dept, nom, nbP);
    if ((strcmp(dept, l->departement)) < 0)
        return ajouterEnTeteDept(l, dept, nom, nbP);
    if ((strcmp(dept, l->departement)) == 0)
        return l;
    l->suiv = insererDept(l->suiv, dept, nom, nbP);
    return l;
}

ListeDept supprimerEnTêteDept(ListeDept l)
{
    MaillonDept *aux;
    if (l == NULL)
    {
        printf("suppression interdite\n");
        exit(1);
    }
    if (l->suiv == NULL)
        return NULL;
    aux = l;
    l = l->suiv;
    free(aux);
    return l;
}

ListeDept supprimerDept(ListeDept l, char *code)
{
    if (l == NULL)
        return l;
    if ((strcmp(code, l->departement)) < 0)
        return l;
    if ((strcmp(code, l->departement)) == 0)
        return supprimerEnTêteDept(l);
    l->suiv = supprimerDept(l->suiv, code);
    return l;
}

ListeDept rechercheDept(ListeDept l, char code[])
{
    if (l == NULL)
        return NULL;
    if ((strcmp(code, l->departement)) < 0)
        return NULL;
    if (strcmp(code, l->departement) == 0)
        return l;
    return rechercheDept(l->suiv, code);
}

void SauvegarderIUT(VilleIUT **tabV, int tailleL)
{
    int i, nbDept;
    FILE *flot;
    flot = fopen("res.txt", "w");
    for (i = 0; i < tailleL; i++)
    {
        fprintf(flot, "%s\n", tabV[i]->ville);
        nbDept = longueurListe(tabV[i]->idDept);
        fprintf(flot, "%d\n", nbDept);
        SauvegarderListe(tabV[i]->idDept, flot);
        free(tabV[i]);
    }
    fclose(flot);
}

void SauvegarderListe(ListeDept l, FILE *flot)
{
    MaillonDept *tmp;
    while (l->suiv != NULL)
    {
        tmp = l;
        fprintf(flot, "%s %d %s\n", l->departement, l->nbPers, l->responsable);
        l = l->suiv;
        free(tmp);
    }
    fprintf(flot, "%s %d %s\n", l->departement, l->nbPers, l->responsable);
    free(l);
}

int longueurListe(ListeDept l)
{
    int cpt = 0;
    while (l != NULL)
    {
        cpt += 1;
        l = l->suiv;
    }
    return cpt;
}


void SauvegardeCandid(Candidat **tabC, int nbCandid)
{
    int i,j, nbIUT;
    FILE *flot;
    flot = fopen("Candidres.txt", "w");
    fprintf(flot,"%d\n", nbCandid);
    for (i = 0; i < nbCandid; i++)
    {
        printf("1\n");
        fprintf(flot, "%s %s\n", tabC[i]->prenom, tabC[i]->nom);
        printf("2\n");
        fprintf(flot, "%.1f %.1f %.1f %.1f\n", tabC[i]->note[0], tabC[i]->note[1], tabC[i]->note[2], tabC[i]->note[3]);
        printf("3\n");
        nbIUT = longueur(tabC[i]->idCandIUT);
        printf("4\n");
        fprintf(flot,"%d\n",nbIUT);
        printf("5\n");
        SauvegarderCandidIUT(tabC[i]->idCandIUT, flot);
        printf("100\n");
        free(tabC[i]);
    }
    system("clear");
    fclose(flot);
}

void SauvegarderCandidIUT(ListeCandid l, FILE *flot)
{
    MaillonCandid *tmp;
    if (tmp == NULL)
    {
        printf("Pb Malloc \n");
        exit(1);
    }
    while(l != NULL)
    {
        tmp = l;
        printf("6\n");
        fprintf(flot, "%s\n%d\n", l->iutCandid, l->nbChoix);
        printf("7\n");
        SauvegardeCandidDept(l->tabDept, l->nbChoix, flot);
        printf("50\n");
        l = l->suiv;
        free(tmp);
    }
    free(l);
}

void SauvegardeCandidDept(ChoixDept **tDept, int nbDept, FILE *flot)
{
    int i;
    for(i = 0; i < nbDept; i++)
    {
        printf("8\n");
        fprintf(flot, "%s\n%d\n%d\n", tDept[i]->departement, tDept[i]->decisionDept, tDept[i]->decisionCandid);
        printf("9\n");
    }
}


int verifChefDepart(VilleIUT **tiut, int nbEle, char *dept, char *ville, char *nom)
{
    int i, verif = 0, test;
    ListeDept l;
    for (i = 0; i < nbEle; i++)
    {
        if (strcmp(tiut[i]->ville, ville) == 0)
        {
            l = tiut[i]->idDept;
            while (l != NULL)
            {
                if (strcmp(l->departement, dept) == 0)
                {
                    if (strcmp(l->responsable, nom) == 0)
                    {
                        verif = 1;
                    }
                }
                l = l->suiv;
            }
        }
    }
    return verif;
}

int menuResponsableAffiche(void)
{
    int choix;
    printf("####################################################\n");
    printf("\t\t|Menu Responsable|\n\n");
    printf("\t\t------------------\n\n");
    printf("1 - Traiter les candidatures (output dans des fichiers)\n");
    printf("\n\n9 - Repasser en mode Utilisateur");
    printf("\n\n10 - Quitter\n");
    printf("####################################################\n");
    printf("\nSelection : ");
    scanf("%d", &choix);
    return choix;
}

int gestionResponsable(VilleIUT **tiut, int nbEle, Candidat **tcandid, int tailleL)
{
    int choix, codeRet, nb;
    char dept[30];
    char ville[30];
    char nom[30];
    Candidat** tabCandidV;
    printf("Nom du département concernée : ");
    scanf("%s%*c", dept);
    printf("%s\n",dept);
    printf("\nNom de la ville concernée : ");
    scanf("%s%*c", ville);
    printf("%s\n",ville);
    printf("\nNom du responsable du département : ");
    fgets(nom, 30, stdin);
    nom[strlen(nom) - 1] = '\0';
    printf("%s",nom);
    printf("\n");
    codeRet = verifChefDepart(tiut, nbEle, dept, ville, nom);
    if (codeRet == 0)
        return 0;
    while (1)
    {
        system("clear");
        choix = menuResponsableAffiche();
        system("clear");
        switch (choix)
        {
        case 1:
            tabCandidV = candidDept(tcandid, dept, ville, tailleL,&nb);
            examinerCandid(tabCandidV, nb, dept, ville);
            system("clear");
            break;
        case 9:
            return 0;
        case 10:
            return -1;
        }
    }
}

int traiterCandidIUTDept(Candidat *candid, char *dept, char *ville)
{
    int i, verif = 0, nb = 0;
    ListeCandid l = candid->idCandIUT;
    while (l != NULL)
    {
        if (strcmp(l->iutCandid, ville) == 0)
        {
            verif = 1;
            break;
        }
        l = l->suiv;
    }
    if (verif == 1)
    {
        for (i = 0; i < l->nbChoix; i++)
        {
            if (strcmp(l->tabDept[i]->departement, dept) == 0)
                return 1;
        }
    }
    return 0;
}

Candidat** candidDept(Candidat** tabCandidat, char* dept, char* ville, int tailleL, int* nb)
{
    int tphys = 50, i, codeRetour;
    Candidat **tabCandDept;
    tabCandDept = (Candidat **)malloc(sizeof(Candidat *) * tphys);
    if (tabCandDept == NULL)
    {
        printf("Pb malloc tab candid\n");
        exit(1);
    }
    *nb = 0;
    for (i = 0; i < tailleL; i++)
    {
        codeRetour = traiterCandidIUTDept(tabCandidat[i], dept, ville);
        if (codeRetour == 1)
        {
            tabCandDept[*nb] = tabCandidat[i];
            *nb += 1;
        }
    }
    return tabCandDept;
}

void SauvegardeCandidAdmis(Candidat **tab, int nb, int admis)
{
    FILE *flot;
    int i;
    float moy;
    if (admis == 1)
        flot = fopen("candidAdmis.txt", "w");
    else
        flot = fopen("candidMEA.txt", "w");
    if (flot == NULL)
    {
        printf("Problème ouverture fichier candidat admis\n");
        exit(1);
    }
    for (i = 0; i < nb; i++)
    {
        moy = (tab[i]->note[0] + tab[i]->note[1] + tab[i]->note[2] + tab[i]->note[3]) / 4;
        fprintf(flot, "%s %s %.1f\n", tab[i]->nom, tab[i]->prenom, moy);
    }
    fclose(flot);
}

void examinerCandid(Candidat **tabCandid, int nb, char* dept, char* ville)
{
    int nbAdmis, i, comptAdmis = 0, j, comptAttente = 0;
    float mini, moy;
    Candidat *tabAdmis[50];
    Candidat *tabEnAttente[50];
    ListeCandid l;
    printf("Nombre d'admis ?\n");
    scanf("%d", &nbAdmis);
    printf("Note minimum ?\n");
    scanf("%f", &mini);
    for (i = 0; i < nb; i++)
    {
        moy = (tabCandid[i]->note[0] + tabCandid[i]->note[1] + tabCandid[i]->note[2] + tabCandid[i]->note[3]) / 4;
        l = tabCandid[i]->idCandIUT;
        while (l != NULL)
        {
            if (strcmp(l->iutCandid, ville) == 0)
                break;
            l = l->suiv;
        }
        for (j = 0; j < l->nbChoix; j++)
        {
            if (strcmp(l->tabDept[j]->departement, dept) == 0)
            {
                if (moy >= mini)
                {
                    if (comptAdmis < nbAdmis)
                    {
                        l->tabDept[j]->decisionDept = 1;
                        tabAdmis[comptAdmis] = tabCandid[i];
                        comptAdmis += 1;
                    }
                    else
                    {
                        l->tabDept[j]->decisionDept = 2;
                        tabEnAttente[comptAttente] = tabCandid[i];
                        comptAttente += 1;
                    }
                }
                else
                    l->tabDept[j]->decisionDept = -1;
            }
        }
    }
    SauvegardeCandidAdmis(tabAdmis, comptAdmis, 1);
    SauvegardeCandidAdmis(tabEnAttente, comptAttente, 0);
}


void afficherAdmiCandid(Candidat **tab,int nbCandid)
{
    int id, pos, trouve, i;
    MaillonCandid *aux;
    while(1)
    {
        printf("Quel est votre identifiant : ");
        scanf("%d", &id);
        pos = rechCandid(tab, nbCandid, id, &trouve);
        if (trouve == 0)
            printf("identifiant absent de la base de donnée\n");
        if (trouve == 1)
        {
            printf("Admition :\n");
            aux = tab[pos]->idCandIUT;
            while(aux != NULL)
            {
                printf("\t %s :\n", aux->iutCandid);
                for (i=0; i < aux->nbChoix; i++)
                {
                    printf("- %s\t", aux->tabDept[i]);
                    if (aux->tabDept[i]->decisionDept == 0)
                        printf("Candidature pas encore traité\n");
                    if (aux->tabDept[i]->decisionDept == 1)
                        printf("Candidature Accepter\n");
                    if (aux->tabDept[i]->decisionDept == -1)
                        printf("Candidature refusée\n");
                    if (aux->tabDept[i]->decisionDept == 2)
                        printf("Candidature en fil d'attente\n");
                }
                aux = aux->suiv;
            }
        if(!verifSelection())
            break;
        }

    }
}

void afficher1AdmiCandid(Candidat **tab,int nbCandid, int id, int pos)
{
    int i;
    MaillonCandid *aux;
    printf("Admition :\n");
    aux = tab[pos]->idCandIUT;
    while(aux != NULL)
    {
        printf("\t %s :\n", aux->iutCandid);
        for (i=0; i < aux->nbChoix; i++)
        {
            printf("- %s\t", aux->tabDept[i]);
            if (aux->tabDept[i]->decisionDept == 0)
                printf("Candidature pas encore traité\n");
            if (aux->tabDept[i]->decisionDept == 1)
                printf("Candidature Accepter\n");
            if (aux->tabDept[i]->decisionDept == -1)
                printf("Candidature refusée\n");
            if (aux->tabDept[i]->decisionDept == 2)
                printf("Candidature en fil d'attente\n");
        }
        aux = aux->suiv;
    }
}

void rejeterVoeux(ListeCandid l, char ville[], int pos)
{
    MaillonCandid *aux;
    aux = l;
    int i=0;
    while(aux == NULL)
    {   
        if (aux->tabDept[pos]->decisionCandid == 1 && strcmp(aux->iutCandid, ville) == 0)
        {
            for(i = 0; i < aux->nbChoix; i++)
            {
                if(aux->tabDept[pos]->decisionCandid == 0)
                    free(aux->tabDept[i]);
            }
            aux = l;
            while(aux != NULL)
            {
                if(strcmp(aux->iutCandid, ville)==0)
                {
                    aux = aux->suiv;
                }
                else suppressionCandid(aux, aux->iutCandid, aux->tabDept, aux->nbChoix);
            }
        }
    }
}


int decision(void)
{
    int choix;
    printf("1 - Accepté\n");
    printf("2 - Refusé\n");
    printf("3 - Rien\n");
    printf("Choix : ");
    scanf("%d", &choix);
    return choix;
}

void gererAdmiCandid(Candidat **tab, int nbCandid)
{
    int id, pos, trouve, i, irech;
    char ville[30], dept[30], choix;
    MaillonCandid *aux;
    while(1)
    {
        printf("Quel est votre identifiant : ");
        scanf("%d", &id);
        pos = rechCandid(tab, nbCandid, id, &trouve);
        if (trouve == 0)
            printf("identifiant absent de la base de donnée\n");
        if (trouve == 1)
        {
            while(1)
            {
                system("clear");
                afficher1AdmiCandid(tab, nbCandid, id, pos);
                while(1)
                {
                    printf("Dans quel IUT voulez-vous gérer votre Candidature : ");
                    scanf("%s%*c", ville);
                    aux = tab[i]->idCandIUT;
                    aux = rechCandidIUT(aux, ville);
                    if (aux == NULL)
                        printf("Vous n'avez pas candidater dans cette IUT\n");
                    if (aux != NULL)
                    {
                        while(1)
                        {
                            printf("Dans quel Département voulez-vous gérer votre Candidature : ");
                            scanf("%s%*c", dept);
                            irech = recherchecandidDept(aux->tabDept, aux->nbChoix, dept, &trouve);
                            if (trouve == 0)
                                printf("Vous n'avez pas postuler dans ce département\n");
                            if (trouve == 1)
                            {
                                while(1)
                                {
                                    choix = decision();
                                    if (choix == 1 && aux->tabDept[irech]->decisionDept == 1)
                                    {
                                        printf("Voeux accepter\n");
                                        aux->tabDept[irech]->decisionCandid = 1;
                                        rejeterVoeux(tab[i]->idCandIUT, ville, irech);
                                        aux->tabDept[0] =  aux->tabDept[irech];
                                        aux->nbChoix = 1;
                                    }
                                    if (choix == 2)
                                    {
                                        if(confirmationSup())
                                        {
                                            free(aux->tabDept[irech]);
                                            decalageGaucheDept(aux->tabDept, irech, aux->nbChoix);
                                            aux->nbChoix = aux->nbChoix;
                                            if (aux->nbChoix == 0)
                                                tab[i]->idCandIUT = suppressionCandid(tab[i]->idCandIUT, ville, aux->tabDept, aux->nbChoix);
                                            printf("voeux supprimé !\n");
                                        }
                                        else break;
                                    }
                                    if (choix == 3)
                                        break;
                                }
                            }
                            if (!verifSelection())
                                return;
                        }
                    }
                    if(!verifSelection())
                        return;
                }
                if(!verifSelection())
                    return;
                
            }
        }
        if (!verifSelection())
            return;
    }
}


